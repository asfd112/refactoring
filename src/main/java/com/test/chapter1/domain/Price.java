package com.test.chapter1.domain;

/**
 * 价格
 * @author xuwenjie
 * @date 15:26 2020/11/10
 **/
public abstract class Price {
    /**
     * 获取类型
     * @author xuwenjie
     * @date 15:27 2020/11/10
     **/
    abstract int getPriceCode();

    /**
     * 金额计算
     * @author xuwenjie
     * @date 14:50 2020/11/10
     **/
    abstract double getCharge(int daysRented);

    /**
     * 常客积分计算
     * @author xuwenjie
     * @date 14:50 2020/11/10
     **/
    public int getFrequentRenterPoints(int daysRented) {
        return 1;
    }
}
