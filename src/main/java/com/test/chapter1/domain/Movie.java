package com.test.chapter1.domain;

/**
 * 影片
 * @author xuwenjie
 * @date 11:28 2020/11/10
 **/
public class Movie {
    public static final int CHILDRENS = 2;
    public static final int REGULAR = 0;
    public static final int NEW_RELEASE = 1;

    private String _title;
    private Price _price;

    public Movie(String title, int priceCode) {
        _title = title;
        setPriceCode(priceCode);
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        this._title = title;
    }

    public int getPriceCode() {
        return _price.getPriceCode();
    }

    public void setPriceCode(int arg) {
        switch (arg) {
            case REGULAR:
                _price = new RegularPrice();
                break;
            case CHILDRENS:
                _price = new ChildrendsPrice();
                break;
            case NEW_RELEASE:
                _price = new NewReleasePrice();
                break;
            default:
                throw new IllegalArgumentException("Incorrect Price Code!");
        }
    }

    /**
     * 金额计算
     * @author xuwenjie
     * @date 14:50 2020/11/10
     **/
    double getCharge(int daysRented) {
        return _price.getCharge(daysRented);
    }

    /**
     * 常客积分计算
     * @author xuwenjie
     * @date 14:50 2020/11/10
     **/
    public int getFrequentRenterPoints(int daysRented) {
        return _price.getFrequentRenterPoints(daysRented);
    }
}
