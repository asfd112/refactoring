package com.test.chapter1.domain;

/**
 * 租赁
 * @author xuwenjie
 * @date 11:31 2020/11/10
 **/
public class Rental {
    private Movie _movie;
    private int _daysRented;

    public Rental(Movie movie, int daysRented) {
        _movie = movie;
        _daysRented = daysRented;
    }

    public Movie getMovie() {
        return _movie;
    }

    public int getDaysRented() {
        return _daysRented;
    }

    /**
     * 金额计算
     * @author xuwenjie
     * @date 14:50 2020/11/10
     **/
    double getCharge() {
        return _movie.getCharge(_daysRented);
    }

    /**
     * 常客积分计算
     * @author xuwenjie
     * @date 14:50 2020/11/10
     **/
    public int getFrequentRenterPoints() {
        return _movie.getFrequentRenterPoints(_daysRented);
    }
}
