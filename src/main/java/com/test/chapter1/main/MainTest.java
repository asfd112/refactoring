package com.test.chapter1.main;

import com.test.chapter1.domain.Customer;
import com.test.chapter1.domain.Movie;
import com.test.chapter1.domain.Rental;

public class MainTest {
    public static void main(String[] args) {
        Customer customer = new Customer("小明");

        Movie movie = new Movie("新片", Movie.NEW_RELEASE);
        Rental rental = new Rental(movie, 5);

        Rental rental1 = new Rental(new Movie("儿童片", Movie.CHILDRENS), 3);
        Rental rental2 = new Rental(new Movie("普通片", Movie.REGULAR), 10);
        customer.addRental(rental);
        customer.addRental(rental1);
        customer.addRental(rental2);
        String statement = customer.statement();

        String consult = "Rental Record For 小明\n" +
                "\t新片\t15.0\n" +
                "\t儿童片\t1.5\n" +
                "\t普通片\t14.0\n" +
                "Amount owed is 30.5\n" +
                "You earned 4 frequent renter points";
        if(consult.equals(statement)) {
            System.out.println("ok");
        }else {
            System.out.println(statement);
        }
    }
}
